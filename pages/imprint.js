import Head from 'next/head';
import styles from '../styles/Home.module.css';
import Footer from '../components/Footer';

export default function Imprint() {
  return (
      <div className={styles.container}>
        <Head>
          <title>BITGRIP next.js Dummy</title>
        </Head>
        <main className={styles.main}>
          <h1 className={styles.title}>
            Imprint
          </h1>
          <p>
            Lorem ipsum.
          </p>
        </main>
        <Footer />
      </div>
  );
}
