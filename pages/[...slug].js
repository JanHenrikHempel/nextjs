import PageLayout from '../components/PageLayout';
import PageContent from '../components/PageContent';
import {withRouter} from 'next/router';
import {fetchCoreMediaPage} from '../apiclients/coremedia';

export async function getStaticProps({req, pathname, query, params}) {
  const slug = params.slug.join('/');

  const pageContent = await fetchCoreMediaPage(slug);

  return {
    props: {
      pageContent: pageContent,
      path: slug,
    },
    revalidate: 60,
  };
}

export async function getStaticPaths() {
  // return { paths: [], fallback: true };
  let paths = [
    {params: {slug: ['demo-en-de', 'test-rte-266856']}},
    {params: {slug: ['demo-en-de6', 'content-elements', 'section-examples']}},
  ];
  return {paths: paths, fallback: true};
}

function CoreMediaPage({pageContent, path}) {
  if (!path) {
    return (<div>No path given.</div>);
  }
  if (!pageContent) {
    return (<div>No content found at CoreMedia for "{path}".</div>);
  }

  return (
      <PageLayout>
        <pre>{JSON.stringify(path, null, 2)}</pre>
        <PageContent sections={pageContent.grid.placements}/>
      </PageLayout>
  );
}

export default withRouter(CoreMediaPage);

/*
<pre>{JSON.stringify(pageContent, null, 2)}</pre>
 */
