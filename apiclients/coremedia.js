export async function fetchCoreMediaPage(path) {
  if (!path) {
    console.error('No path given.');
    return false;
  }

  const coreMediaApiBaseUrl = 'https://preview-api-ci2.ksb.bitgrip.berlin/api/v1/page/';
  const url =
      coreMediaApiBaseUrl + path;
  const fetchParams = {
    method: 'GET',
  };

  const response = await fetch(url, fetchParams);
  if (response.status !== 200) {
    console.warn(
        'Could not fetch article with path ',
        path,
        'status',
        response.status,
    );
    return false;
  }

  const json = await response.json();
  if (!json) {
    console.error(
        'Response fetched for page with path is missing or invalid',
        path,
        'status',
        response.status,
    );
    return false;
  }

  // console.log(json);

  return json;
}
