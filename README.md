This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Setup

```bash
yarn
```

## Development Mode

```bash
yarn dev
```
Open [http://localhost:3000](http://localhost:3000).

## Serve locally

```bash
yarn build
yarn start
```

Open [http://localhost:3000](http://localhost:3000).

Static files are placed as html and json here:

![docs/static-files.jpg](docs/static-files.jpg)

