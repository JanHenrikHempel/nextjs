import Link from 'next/link';

function Footer() {
  return (
      <footer>
        <img
            src="https://www.magnolia-cms.com/.imaging/mte/corporate-website-blogs-backup-theme/1080/dam/logos/partners-app/abc/logo-bitgrip-2020-01-14.png/jcr:content/logo-bitgrip-2020-01-14.png"
            alt="BITGRIP Logo" className="logo"/>
        <Link href="/imprint">
          <a>Imprint</a>
        </Link>
        <Link href="/demo-en-de/test-rte-266856">
          <a>First CoreMedia Page</a>
        </Link>
        <Link href="/demo-en-de/content-elements/section-examples">
          <a>Second CoreMedia Page</a>
        </Link>



      </footer>
  );
}

export default Footer;
