const Placeholder = ({ componentName }) => (
  <div>The paragraph {componentName} has not been created yet.</div>
);

export default Placeholder;
