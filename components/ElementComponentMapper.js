import Placeholder from './Placeholder';
import Article from './Article';

const Elements = {
  'CMArticle': Article
};

const ElementComponentMapper = ({element}) => {
  if (!element) {
    console.log(
        'Component ElementComponentMapper could not map element, because "element" is missing.',
    );
    return <></>;
  }

  if (!element.name) {
    console.log(
        'Component ElementComponentMapper could not map element, because "element.component" is not set.',
    );
    return <></>;
  }
  if (typeof Elements[element.type] !== 'undefined') {
    const Element = Elements[element.type];
    let content = null;

    // preprocessing incoming data and map to components
    switch (element.type) {
      case 'CMArticle':
        element = element;
        break;
      default:
        content = {};
    }

    return (
        <div className={element.name}>
          <Element content={element}/>
          <hr/>
        </div>
    );
  }

  return (
      <Placeholder componentName={element.name}/>
  );
};

export default ElementComponentMapper;
