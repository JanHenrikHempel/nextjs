import ElementComponentMapper from './ElementComponentMapper';

function PageContent({sections}) {
  if (!sections) {
    console.info(
        'Component PageContent not rendered, because it has no sections.');
    return <></>;
  }

  return (
      <div>
        {sections.map((section, sectionCounter) => {
          if (section) {
            return (
                <section key={'section-'+sectionCounter}>
                  <h2>{section.name}</h2>
                  {section.items && section.items.length > 0 &&
                  section.items.map((element, elementCounter) => {
                    return (
                        <div key={'element-'+elementCounter}>
                          <ElementComponentMapper element={element}/>
                          <pre>{JSON.stringify(element, null, 2)}</pre>
                        </div>
                    );
                  })}
                </section>
            );
          }
        })}
      </div>
  );
}

export default PageContent;
