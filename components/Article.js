import {Markup} from 'interweave';

function Article({content}) {
  const firstPicture = content?.pictures[0];
  const imageSrc = firstPicture?.source;

  return (
      <article>
        <h3>Article: {content.name}</h3>
        {imageSrc && <img src={imageSrc} />}
        <div>
          <Markup
              content={content.detailText}
              noWrap='true'
              allowList={['p', 'i', 'em', 'sub', 'sup', 'del', 'br']}
          />
        </div>
      </article>
  );
}

export default Article;
