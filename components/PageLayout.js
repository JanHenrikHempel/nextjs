import Footer from './Footer';

function PageLayout(props) {
  if (!props || !props.children) {
    console.info('Component PageLayout not rendered, because "props" are missing.');
    return <></>;
  }

  return (
      <div>
        <Footer/>
        {props.children}
      </div>
  );
}

export default PageLayout;
